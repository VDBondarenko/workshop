<?php

namespace App\Tests;

use Symfony\Component\Panther\PantherTestCase;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class E2eTest extends WebTestCase
{
    public function testMyApp(): void {
        $client = $this->makeClient();
        $crawler = $client->request('GET', '/books');
        $this->assertEquals(4, $crawler->filter('h1')->count());
    }
}