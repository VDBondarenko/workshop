FROM php:7.2-fpm

ARG WEB_USER
ARG WEB_GROUP
ARG PHP_ROOT_DIR

COPY www.conf ${PHP_ROOT_DIR}/php-fpm.d/www.conf
COPY php.ini ${PHP_ROOT_DIR}/php/php.ini

RUN docker-php-ext-install pdo_mysql
RUN pecl install apcu
RUN apt-get update && apt-get install -y \
        apt-utils \
        libmemcached-dev  \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        git \
        libxml2-dev \
        libzip-dev \
        zip \
        && pecl install xdebug \
        && pecl install memcached \
        && docker-php-ext-enable xdebug \
        && docker-php-ext-install opcache \
      #  && docker-php-ext-install -j$(nproc) iconv \
      #  && docker-php-ext-install xml \
        && docker-php-ext-install soap \
        && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
        && docker-php-ext-install -j$(nproc) gd \
        && docker-php-ext-configure zip --with-libzip \
        && docker-php-ext-install zip \
        && docker-php-ext-enable memcached


RUN docker-php-ext-enable apcu

#composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --filename=composer \
    && php -r "unlink('composer-setup.php');" \
    && mv composer /usr/local/bin/composer

##nodejs
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
#RUN npm install -g bower

#yarn
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install yarn

RUN mkdir -p /usr/local/etc/logs
RUN touch /usr/local/etc/logs/error.log
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN PATH=$PATH:/usr/src/apps/vendor/bin:bin

RUN usermod -u 1000 ${WEB_USER} \
 && groupmod -g 1000 ${WEB_GROUP} \
 && chown -R ${WEB_USER}:${WEB_GROUP} ${PHP_ROOT_DIR}/php-fpm.d/www.conf

