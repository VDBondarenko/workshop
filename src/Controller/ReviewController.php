<?php

namespace App\Controller;

use App\Entity\Review;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ReviewController extends AbstractController
{
    /**
     * @Route("/api/all-reviews/{id}", name="reviews", requirements={"id"="\d+"})
     */
    public function reviews($id) {
        $reviews = $this->getDoctrine()
            ->getRepository(Review::class)
            ->findBy(['book' => $id]);

        return new JsonResponse($reviews);
    }
}