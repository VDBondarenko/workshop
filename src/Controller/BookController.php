<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Review;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;
use Symfony\Component\HttpFoundation\Response;

final class BookController extends AbstractController
{
    /**
     * @Route("/books", name="books")
     */
    public function books() {
        return $this->render(
            'books.html.twig'
        );
    }

    /**
     * @Route("/book/{id}", name="book", requirements={"id"="\d+"})
     */
    public function getBook(
        Book $book
    ) {
        return $this->render(
            'book.html.twig',
            ['book' => $book]
        );
    }

    /**
     * @Route("/publish/{id}", name="publish", requirements={"id"="\d+"})
     */
    public function publish(Publisher $publisher, $id) {
        $em = $this->getDoctrine()->getManager();
        $book = $this->getDoctrine()
            ->getRepository(Book::class)
            ->find($id);
        $review = new Review();
        $review->book = $book;
        $review->author = 'asd';
        $review->rating = 10;
        $review->publicationDate = new \DateTime();
        $review->body = '!&&hello';
        $em->persist($review);
        $em->flush();

        $update = new Update(
            'http://192.168.1.194:3000/demo/books/2.jsonld',
            json_encode(['listComment' => '???Hello!'])
        );

        // The Publisher service is an invokable object
        $publisher($update);

        return new Response('new comment published');
    }
}