import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        currentJWT: localStorage.getItem('token') || '',
        books: [],
        reviews: []
    },

    getters: {
        books: state => state.books,
        reviews: state => state.reviews,
        jwt: state => state.currentJWT,
        jwtData: (state, getters) => state.currentJWT ? JSON.parse(atob(getters.jwt.split('.')[1])) : null,
    },

    mutations: {
        setJWT(state, jwt) {
            // When this updates, the getters and anything bound to them updates as well.
            state.currentJWT = jwt;
        },
        setBooks(state, books) {
            state.books = books;
        },
        setReviews(state, reviews) {
            state.reviews = reviews.reviews;
        },
        logout(state) {
            state.currentJWT = '';
        }
    },

    actions: {
        async fetchBooks({commit, getters}) {
            const config = {
                headers: {
                    Authorization: "Bearer " + getters.jwt,
                    accept: "application/json"
                }
            };

            axios
                .get(
                    'http://127.0.0.1:81/api/books',
                    config,
                )
                .then(function (response) {
                    commit('setBooks', response.data);
                });
        },
        async fetchJWT({commit}, {username, password}) {
            // Perform the HTTP request.
            axios
                .post(
                    'http://127.0.0.1:81/login_check',
                    {username, password}
                )
                .then(function (response) {
                    localStorage.setItem('token', response.data.token);
                    commit('setJWT', response.data.token);
                });
        },
        async fetchRegistration({commit, dispatch}, {username, password}) {
            axios.post(
                'http://127.0.0.1:81/register',
                {username, password}
            )
                .then(function (response) {
                    dispatch('fetchJWT', {username, password});
                });
        },
        async fetchComments({commit, getters}, {bookId}) {
            const config = {
                headers: {
                    Authorization: "Bearer " + getters.jwt,
                    accept: "application/json"
                }
            };

            axios
                .get(
                    'http://127.0.0.1:81/api/all-reviews/' + bookId,
                    config,
                )
                .then(function (response) {
                    commit('setReviews', {bookId, reviews: response.data});
                });
        },
        logout({commit}) {
            commit('logout');
            localStorage.removeItem('token');
        }
    }
});