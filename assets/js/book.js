import Vue from 'vue'
import Book from './components/Book'
import store from './store/store';

// eslint-disable-next-line no-new
new Vue({
    store,
    el: '#book',
    data: {
        bookId: ''
    },
    template: '<book :bookId="bookId"/>',
    components: { Book },
    beforeMount () {
        this.bookId = this.$el.attributes['data-book-id'].value
    }
});